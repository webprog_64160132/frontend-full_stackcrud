import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useConfirmStore } from "./confirm";

export const useEmployeeStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const confirmStore = useConfirmStore();
  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({ name: "", position: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedEmployee.value = { name: "", position: "" };
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployee();
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        const res = await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        const res = await employeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;

      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก Employee ได้");
    }
    loadingStore.isLoading = false;
  }
  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));
    dialog.value = true;
  }
  return {
    employees,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    editEmployee,
    deleteEmployee,
  };
});
