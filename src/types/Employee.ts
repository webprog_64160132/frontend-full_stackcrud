export default interface Employee {
  id?: number;

  name: string;

  position: string;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
